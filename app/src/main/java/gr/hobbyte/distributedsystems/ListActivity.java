package gr.hobbyte.distributedsystems;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

/**
 * Shows received data in a more readable form
 */
public class ListActivity extends Activity {

    ListView lv;
    private String[][] lvData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        lv = (ListView) findViewById(R.id.listView);
        Intent extras = getIntent();
        Bundle b = extras.getExtras();
        Checkins[] ch = (Checkins[]) b.get("data");

        //if there is no data
        if(ch == null){
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("Data has not been obtained yet.\nPlease connect to server at least once.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();//return to main activity
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }else{
            lvData = putDataToList(ch);//get data in string[] format
            lv.setAdapter(new ListAdapter(this, lvData));//push it to adapter
        }
    }

    /**
     * Convers data to String[] to push to array adapter.
     * @param lvData
     * @return data
     */
    private String[][] putDataToList(Checkins[] lvData){

        String[][] results = new String[lvData.length][4];
        for(int i = 0; i < results.length; i++){
            results[i][0] = Integer.toString(i+1);
            results[i][1] = lvData[i].getPOI().getName();
            results[i][2] = lvData[i].getPOI().getCat();
            results[i][3] = Integer.toString(lvData[i].getPOI().getCounter());
        }
        return results;
    }
}
