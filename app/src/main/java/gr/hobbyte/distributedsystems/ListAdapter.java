package gr.hobbyte.distributedsystems;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * This class is used to utilize the listview adapter with data!
 */
public class ListAdapter extends ArrayAdapter<String[]> {

    String[][] data;

    public ListAdapter(Context context, String[][] resource) {
        super(context, R.layout.list_adapter, resource);
        this.data = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.list_adapter, parent, false);
        TextView number = (TextView) view.findViewById(R.id.LVnumber);
        TextView numberOfCheckins = (TextView) view.findViewById(R.id.LVsum);
        TextView name = (TextView) view.findViewById(R.id.LVname);
        TextView category = (TextView) view.findViewById(R.id.LVcat);
        number.setText(data[position][0]);
        name.setText(data[position][1]);
        category.setText(data[position][2]);
        numberOfCheckins.setText(data[position][3]);
        return view;
    }
}