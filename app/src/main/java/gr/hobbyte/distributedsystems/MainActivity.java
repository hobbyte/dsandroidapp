package gr.hobbyte.distributedsystems;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends FragmentActivity implements View.OnClickListener, OnMapReadyCallback, View.OnLongClickListener {

    private GoogleMap mMap;
    private MarkerOptions options = new MarkerOptions();
    private ArrayList<Marker> markers = new ArrayList<>();
    private TextView date;
    private TextView time;
    private EditText results;
    private int results_int = 10;
    //time picker method counter
    private int time_counter = 0;
    //date picker method counter
    private int date_counter = 0;
    //variables for time
    private int time_hours_start = 0, time_minutes_start = 0, time_hours_end = 23, time_minutes_end = 59;
    //variables for date

    private int date_day_start = 1, date_month_start = 0, date_year_start = 2011, date_day_end, date_month_end, date_year_end;
    public static Checkins[] res;
    private Context c;
    static Button run;

    //variables to store coordinate inputs
    double[] sLong_input = {0.0};
    double[] sLat_input = {0.0};
    double[] eLong_input = {0.0};
    double[] eLat_input = {0.0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c = this;
        date = (TextView) findViewById(R.id.date);
        time = (TextView) findViewById(R.id.time);
        results = (EditText) findViewById(R.id.results);
        run = (Button) findViewById(R.id.run);
        Button manual = (Button) findViewById(R.id.LongLat);
        
        date.setOnClickListener(this);
        time.setOnClickListener(this);
        results.setOnClickListener(this);
        run.setOnClickListener(this);
        manual.setOnClickListener(this);
        run.setOnLongClickListener(this);

        //initiate time text
        final Calendar c = Calendar.getInstance();
        date_year_end = c.get(Calendar.YEAR);
        date_month_end = c.get(Calendar.MONTH);
        date_day_end = c.get(Calendar.DAY_OF_MONTH);

        time.setText(time_hours_start + ":" + time_minutes_start + " - " + time_hours_end + ":" + time_minutes_end);
        date.setText(date_day_start + "/" + (date_month_start + 1) + "/" + date_year_start + " - " + date_day_end + "/" + (date_month_end + 1) + "/" + date_year_end);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()         //check for google play services
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //Toast.makeText(getApplication(), "eee", Toast.LENGTH_SHORT).show();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng NY = new LatLng(40.730610, -73.935242);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(NY));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        mMap.setTrafficEnabled(true);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
            }
        });

    }

    /**
     * take the input from time range
     *
     * @param hour
     * @param minute
     */
    public void Time_Picker(int hour, int minute) {
        if (time_counter == 0) {
            time_hours_start = hour;
            time_minutes_start = minute;
            time_counter = 1;
        } else {
            time_hours_end = hour;
            time_minutes_end = minute;
            time_counter = 0;
        }
        time.setText(time_hours_start + ":" + time_minutes_start + " - " + time_hours_end + ":" + time_minutes_end);
    }

    public void Date_Picker(int day, int month, int year) {
        if (date_counter == 0) {
            date_day_start = day;
            date_month_start = month;
            date_year_start = year;
            date_counter = 1;
        } else {
            date_day_end = day;
            date_month_end = month;
            date_year_end = year;
            date_counter = 0;
        }
        date.setText(date_day_start + "/" + (date_month_start + 1) + "/" + date_year_start + " - " + date_day_end + "/" + (date_month_end + 1) + "/" + date_year_end);
    }

    //Pushes date data to Datamanager
    private DataManager sendDateData() {
        DataManager dm = new DataManager();
        Date start = new Date(date_year_start - 1900, date_month_start, date_day_start, time_hours_start, time_minutes_start);
        Date end = new Date(date_year_end - 1900, date_month_end, date_day_end, time_hours_end, time_minutes_end);
        dm.setDates(start, end);
        return dm;
    }

    @Override
    public void onClick(View v) { // button clicks actions
        switch (v.getId()) {
            case R.id.date:
                //first call for starting date
                DialogFragment dateFragmentstart = new DatePickerFragment();
                dateFragmentstart.show(getSupportFragmentManager(), "datePicker");
                Toast.makeText(getApplication(), "Choose Starting and Ending Date", Toast.LENGTH_SHORT).show();
                //second call for ending date
                DialogFragment dateFragmentend = new DatePickerFragment();
                dateFragmentend.show(getSupportFragmentManager(), "datePicker");
                break;
            case R.id.time:
                //first call for starting time
                DialogFragment timeFragmentstart = new TimePickerFragment();
                timeFragmentstart.show(getSupportFragmentManager(), "timePicker");
                Toast.makeText(getApplication(), "Choose Starting and Ending Time", Toast.LENGTH_SHORT).show();
                //second call for ending time
                DialogFragment timeFragmentend = new TimePickerFragment();
                timeFragmentend.show(getSupportFragmentManager(), "timePicker");
                break;
            case R.id.results:
                Toast.makeText(getApplication(), "Choose how many results to show", Toast.LENGTH_SHORT).show();
                break;
            case R.id.run:
                run.setVisibility(View.GONE);
                mMap.clear();//clear other markers
                DataManager dm = sendDateData();
                try{//get number of results from edittext
                    results_int = Integer.parseInt(results.getText().toString());
                }catch(NumberFormatException nfex){
                    results_int = 10;
                }
                if (sLong_input[0] != 0.0 || sLat_input[0] != 0.0 || eLong_input[0] != 0.0 || eLat_input[0] != 0.0) {
                    dm.getLocation(sLat_input[0], eLat_input[0], eLong_input[0], sLong_input[0], results_int, mMap, options, markers, c);
                } else {
                    Projection projection = mMap.getProjection();
                    LatLng upLeft = projection.getVisibleRegion().farLeft;
                    LatLng downRight = projection.getVisibleRegion().nearRight;
                    dm.getLocation(upLeft.latitude, downRight.latitude, upLeft.longitude, downRight.longitude, results_int, mMap, options, markers, c);
                }
                break;
            case R.id.LongLat:
                ManualInput();
                break;
        }
    }

    /**
     * add manually the coordinates
     */
    private void ManualInput() {

        LayoutInflater li = getLayoutInflater();
        View dialogView = li.inflate(R.layout.dialog, null);

        //variables to take inputs
        final EditText sLong = (EditText) dialogView.findViewById(R.id.sLong_Edit);
        final EditText sLat = (EditText) dialogView.findViewById(R.id.sLat_Edit);
        final EditText eLong = (EditText) dialogView.findViewById(R.id.eLong_Edit);
        final EditText eLat = (EditText) dialogView.findViewById(R.id.eLat_Edit);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(dialogView);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                //read the inputs, if empty make them 0.0
                if (sLong.getText().toString() == null || sLong.getText().toString().isEmpty()) {
                    sLong_input[0] = 0.0;
                } else {
                    sLong_input[0] = Double.parseDouble(sLong.getText().toString());
                }
                if (sLat.getText().toString() == null || sLat.getText().toString().isEmpty()) {
                    sLat_input[0] = 0.0;
                } else {
                    sLat_input[0] = Double.parseDouble(sLat.getText().toString());
                }
                if (eLong.getText().toString() == null || eLong.getText().toString().isEmpty()) {
                    eLong_input[0] = 0.0;
                } else {
                    eLong_input[0] = Double.parseDouble(eLong.getText().toString());
                }
                if (eLat.getText().toString() == null || eLat.getText().toString().isEmpty()) {
                    eLat_input[0] = 0.0;
                } else {
                    eLat_input[0] = Double.parseDouble(eLat.getText().toString());
                }
            }
        });
        //close dialog with cancel
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        alert.show();
    }

    public static void putMarkers(Checkins[] res, GoogleMap mMap, MarkerOptions options, ArrayList<Marker> markers) {
        LatLng temp;
        for (int i = 0; i < res.length; i++) {
            temp = new LatLng(res[i].getPOI().getLat(), res[i].getPOI().getLon());
            options.position(temp);
            options.title(res[i].getPOI().getName());
            options.snippet(res[i].getPOI().getCat() + ", " + res[i].getPOI().getCounter() + " users checked in during your criteria");
            markers.add(mMap.addMarker(options));
        }
        run.setVisibility(View.VISIBLE);
    }

    /**
     * Show listview activity
     * @param view
     * @return
     */
    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.run:

                Intent intent = new Intent(this, ListActivity.class);
                intent.putExtra("data", res);
                startActivity(intent);
                break;
        }
        return true;
    }

    /**
     * choose the time range from a dialog
     */
    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            ((MainActivity) getActivity()).Time_Picker(hourOfDay, minute);
        }
    }

    /**
     * choose the date range from a dialog
     */
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        boolean isDataSet = false;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            ((MainActivity) getActivity()).Date_Picker(day, month, year);
            isDataSet = true;
        }
    }

    public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

        private GoogleMap mMap;
        private MarkerOptions options = new MarkerOptions();
        private ArrayList<LatLng> latlngs = new ArrayList<>();
        private ArrayList<Marker> markers = new ArrayList<>();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()         //check for google play services
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            //Toast.makeText(getApplication(), "eee", Toast.LENGTH_SHORT).show();
        }


        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera. In this case,
         * we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to install
         * it inside the SupportMapFragment. This method will only be triggered once the user has
         * installed Google Play services and returned to the app.
         */
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;

            latlngs.add(new LatLng(37.994129, 23.731960));
            latlngs.add(new LatLng(37.9757, 23.7339));

            int i = 1;
            for (LatLng point : latlngs) {
                options.position(point);
                options.title("POI" + i);
                options.snippet("I'm a snippet! :)");
                markers.add(mMap.addMarker(options));
                i++;
            }

            LatLng athens = new LatLng(37.994129, 23.731960);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(athens));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            mMap.setTrafficEnabled(true);

        }
    }
}
