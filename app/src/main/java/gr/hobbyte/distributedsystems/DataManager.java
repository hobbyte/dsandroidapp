package gr.hobbyte.distributedsystems;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * This class is responsible for sending data to mappers
 * and receiving data from reducers
 */
public class DataManager {

    //copied code
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private Date start;
    private Date end;
    private double sLong = -180, sLat = -90, eLong = 180, eLat = 90;
    private Socket requestSocket = null, requestSocketW = null;
    private ObjectOutputStream out = null;
    private ObjectInputStream in = null;
    private String message;
    private int topK;
    private Boolean ready = false;
    //Mapper credentials
    private String[] mapper_IPs = {"192.168.1.3", "127.0.0.1", "127.0.0.1"};
    private int[] mapper_Ports = {4321, 4321, 4323};
    private Checkins[] res;
    private static final String MAPPER1_IP = "192.168.1.65";
    private static final int MAPPER1_PORT = 1;
    private MarkerOptions options;
    private GoogleMap mMap;
    private ArrayList<Marker> markers;
    private Context c;

    public Checkins[] getResults() {
        return res;
    }

    //puts markers on map
    public void getLocation(double sLat, double eLat, double sLong, double eLong, int topK, GoogleMap gMap, MarkerOptions options, ArrayList<Marker> markers, Context c) {
        this.sLat = sLat;
        this.eLat = eLat;
        this.sLong = sLong;
        this.eLong = eLong;
        this.topK = topK;
        this.options = options;
        this.mMap = gMap;
        this.markers = markers;
        this.c = c;
        mapperAsyncTask mapper = new mapperAsyncTask();
        mapper.execute(mapper_IPs[0], mapper_IPs[1], mapper_IPs[2]);

    }

    /**
     * AsyncTask. Creates a server to wait for reducer result.
     */
    private class waitForReducer extends AsyncTask<String, Void, Checkins[]> {
        ServerSocket serverSocket;
        Socket soc;
        InputStream is;
        ObjectInputStream ois;
        int port = 1547;
        boolean allOk = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Checkins[] doInBackground(String... params) {
            Log.d("ReducerAsyncThread", "Starting doInBackground()");
            try {
                serverSocket = new ServerSocket(port);
                soc = serverSocket.accept();
                is = soc.getInputStream();
                ois = new ObjectInputStream(is);
                res = new Checkins[topK];

                for (int i = 0; i < topK; i++) {
                    res[i] = (Checkins) ois.readObject();
                }
                Log.d("ReducerAsyncThread", "Data received");
                is.close();
                out.close();
                ois.close();
                serverSocket.close();
            } catch (IOException e) {
                allOk = false;
            } catch (ClassNotFoundException e) {
                Log.d("ReducerAsyncThread", "ClassNotFoundException");
            }
            return res;
        }

        @Override
        protected void onPostExecute(Checkins[] s) {
            if (allOk) {
                //put markers on map
                MainActivity.putMarkers(res, mMap, options, markers);
                MainActivity.res = res;
            } else {
                new AlertDialog.Builder(c)
                        .setTitle("Error")
                        .setMessage("Can not connect to server.")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                MainActivity.run.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Sends data to mappers and opens reducer server thread
     */
    private class mapperAsyncTask extends AsyncTask<String, Void, String> {
        boolean[] finish;
        Random rand = new Random();
        int random = rand.nextInt(2000)+4000;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            finish = new boolean[3];
            int i = 0;
            finish[i] = connectToMapper(mapper_IPs, mapper_Ports);
            i++;
            return null;
        }

        /**
         * Opens connection to mappers and sends data
         *
         * @param mapper_ip
         * @param mapper_port
         * @return true if everthing went ok
         */
        private boolean connectToMapper(String[] mapper_ip, int[] mapper_port) {
            boolean cs[] = new boolean[3];
            cs[0] = true;
            cs[1] = true;
            cs[2] = true;
            boolean con[] = new boolean[3];
            con[0] = false;
            con[1] = false;
            con[2] = false;

            double dif = Math.abs(sLat - eLat) / 3;
            double[] lats = new double[4];
            lats[0] = sLat;
            lats[1] = sLat - dif;
            lats[2] = sLat - 2 * dif;
            lats[3] = eLat;

            ObjectOutputStream outW, outR;
            try  {
                Socket socR = new Socket();
                socR.connect(new InetSocketAddress(mapper_ip[0], mapper_port[0]-100), 1000);
                outR = new ObjectOutputStream(socR.getOutputStream());
                outR.writeInt(random);
                outR.writeInt(1);
                outR.flush();
            } catch (IOException e) {
                return false;
            }
            try (Socket soc = new Socket()) {
                Socket socW = new Socket();
                socW.connect(new InetSocketAddress(mapper_ip[0], mapper_port[0]-100), 1000);
                outW = new ObjectOutputStream(socW.getOutputStream());
                outW.writeInt(random);
                outW.writeInt(0);
                outW.flush();
                soc.connect(new InetSocketAddress(mapper_ip[0], mapper_port[0]), 1000);
                boolean temp1 = connection(soc, lats, 0);
                cs[0] = temp1;
                con[0] = temp1;
            } catch (IOException e) {
                cs[0] = false;
            }
            try (Socket soc = new Socket()) {
                Socket socW = new Socket();
                socW.connect(new InetSocketAddress(mapper_ip[1], mapper_port[1]-100), 1000);
                outW = new ObjectOutputStream(socW.getOutputStream());
                outW.writeInt(random);
                outW.writeInt(0);
                outW.flush();
                soc.connect(new InetSocketAddress(mapper_ip[1], mapper_port[1]), 1000);
                boolean temp2 = connection(soc, lats, 1);
                cs[1] = temp2;
                con[1] = temp2;
            } catch (IOException e) {
                cs[1] = false;
            }
            try (Socket soc = new Socket()) {
                Socket socW = new Socket();
                socW.connect(new InetSocketAddress(mapper_ip[2], mapper_port[2]-100), 1000);
                outW = new ObjectOutputStream(socW.getOutputStream());
                outW.writeInt(random);
                outW.writeInt(0);
                outW.flush();
                soc.connect(new InetSocketAddress(mapper_ip[2], mapper_port[2]), 1000);
                boolean temp3 = connection(soc, lats, 2);
                cs[2] = temp3;
                con[2] = temp3;
            } catch (IOException e) {
                cs[2] = false;
            }
            while (true) {
                if (!con[0]) {
                    int toTry;
                    if (cs[1]) toTry = 1;
                    else if (cs[2]) toTry = 2;
                    else return false;
                    try (Socket soc = new Socket()) {
                        Socket socW = new Socket();
                        socW.connect(new InetSocketAddress(mapper_ip[toTry], mapper_port[toTry]-100), 1000);
                        outW = new ObjectOutputStream(socW.getOutputStream());
                        outW.writeInt(random);
                        outW.writeInt(0);
                        outW.flush();
                        soc.connect(new InetSocketAddress(mapper_ip[toTry], mapper_port[toTry]), 1000);
                        boolean temp1 = connection(soc, lats, 0);
                        cs[toTry] = temp1;
                        con[0] = temp1;
                    } catch (IOException e) {
                        cs[toTry] = false;
                    }
                } else if (!con[1]) {
                    int toTry;
                    if (cs[0]) toTry = 0;
                    else if (cs[2]) toTry = 2;
                    else return false;
                    try (Socket soc = new Socket()) {
                        Socket socW = new Socket();
                        socW.connect(new InetSocketAddress(mapper_ip[toTry], mapper_port[toTry]-100), 1000);
                        outW = new ObjectOutputStream(socW.getOutputStream());
                        outW.writeInt(random);
                        outW.writeInt(0);
                        outW.flush();
                        soc.connect(new InetSocketAddress(mapper_ip[toTry], mapper_port[toTry]), 1000);
                        boolean temp1 = connection(soc, lats, 1);
                        cs[toTry] = temp1;
                        con[1] = temp1;
                    } catch (IOException e) {
                        cs[toTry] = false;
                    }
                } else if (!con[2]) {
                    int toTry;
                    if (cs[0]) toTry = 0;
                    else if (cs[1]) toTry = 1;
                    else return false;
                    try (Socket soc = new Socket()) {
                        Socket socW = new Socket();
                        socW.connect(new InetSocketAddress(mapper_ip[toTry], mapper_port[toTry]-100), 1000);
                        outW = new ObjectOutputStream(socW.getOutputStream());
                        outW.writeInt(random);
                        outW.writeInt(0);
                        outW.flush();
                        soc.connect(new InetSocketAddress(mapper_ip[toTry], mapper_port[toTry]), 1000);
                        boolean temp1 = connection(soc, lats, 2);
                        cs[toTry] = temp1;
                        con[2] = temp1;
                    } catch (IOException e) {
                        cs[toTry] = false;
                    }
                } else break;
            }
            return true;
        }

        private boolean connection(Socket socket, double lats[], int map_num) {
            try {
                out = new ObjectOutputStream(socket.getOutputStream());
                in = new ObjectInputStream(socket.getInputStream());
                message = (String) in.readObject();
                System.out.println("Server> " + message);
                out.writeObject(start);
                out.writeObject(end);
                out.writeDouble(sLong);
                out.writeDouble(lats[map_num]);
                out.writeDouble(eLong);
                out.writeDouble(lats[map_num + 1]);
                out.writeInt(topK);
                out.flush();

                try {

                    in.close();
                    out.close();
                    socket.close();

                } catch (IOException i) {
                    Log.d("MapperCon", "Can not close connection. Was it open?");
                    return false;
                }
                return true;
            } catch (IOException e) {
                return false;
            } catch (ClassNotFoundException c) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (!finish[0]/* || !finish[1] || !finish[2]*/) {
                new AlertDialog.Builder(c)
                        .setTitle("Error")
                        .setMessage("Can not connect to server.")
                        .setPositiveButton("OK", null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                MainActivity.run.setVisibility(View.VISIBLE);
            } else {
                //start reducer
                waitForReducer red = new waitForReducer();
                red.execute();
            }
        }
    }

    public static void waitForMappers() {
    }

    public static void ackToReducers() {

    }

    public void setDates(Date start, Date end) {
        this.start = start;
        this.end = end;
    }
}
