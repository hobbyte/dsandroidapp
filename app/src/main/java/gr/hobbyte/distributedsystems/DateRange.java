/*
Athens University of Economics and Business
Course: Distributed Systems

David Angelos (3130049)
Giotas Konstantinos (3130040)
Gketsoppoulos Petros (3130041)
 */
package gr.hobbyte.distributedsystems;

import java.io.Serializable;
import java.util.Date;

public class DateRange implements Serializable{

    private static final long serialVersionUID = 12345678905L;

    private Date start, end;

    DateRange(Date start, Date end ){
        super();
        this.start = start;
        this.end = end;
    }

    Date getStartDate(){
        return start;
    }

    public void setStartDate(Date start){
        this.start = start;
    }

    public Date getEndDate(){
        return end;
    }

    public void setEndDate(Date end){
        this.end = end;
    }

    public String toString(){
        return start + " - " + end;
    }
}